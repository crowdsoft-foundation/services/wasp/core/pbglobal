from .abstract import Event
import datetime
import hashlib

class statisticalDataAdded(Event):
    def __init__(self, consumer=None, record_type=None, record_data=None, login=None, skip_validation=False):
        super().__init__()
        self.record_id = None
        self.correlation_id = ""
        self.owner = consumer
        self.creator = consumer
        self.consumer = consumer
        self.login = login
        self.record_type = record_type
        self.record_data = record_data

        if not skip_validation:
            self.validate()

    def validate(self):
        assert self.consumer is not None, "consumer may not be empty"
        assert self.record_type is not None, "record_type may not be empty"
        assert self.record_data is not None, "record_data may not be empty"

        return True
