from .abstract import Event


class calendarRessourceUpdated(Event):
    def __init__(self,
                 ressource_id=None,
                 display_name=None,
                 type="person",
                 props=None,
                 correlation_id=None,
                 owner=None,
                 creator=None,
                 skip_validation=False
                 ):
        super().__init__()

        self.ressource_id = ressource_id
        self.display_name = display_name
        self.type = type
        self.props = props
        self.correlation_id = correlation_id
        self.owner = owner
        self.creator = creator

        if not skip_validation:
            self.validate()


    def validate(self):
        assert self.display_name is not None, "display_name may not be empty"
        assert self.ressource_id is not None, "ressource_id may not be empty"

        return True
