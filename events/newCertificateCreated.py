from .abstract import Event
import datetime
import hashlib

class newCertificateCreated(Event):
    def __init__(self, consumer=None, public=None, private=None, addedtime=None, correlation_id=None,
                 skip_validation=False):
        super().__init__()
        self.owner = consumer
        self.creator = consumer
        self.public = public
        self.private = private
        self.addedtime = addedtime

        self.correlation_id = correlation_id
        if not skip_validation:
            self.validate()

    def validate(self):
        assert self.owner is not None, "consumer may not be empty"
        assert self.public is not None, "public may not be empty"
        assert self.private is not None, "private may not be empty"
        assert self.correlation_id is not None, "correlation_id may not be empty"
        return True
