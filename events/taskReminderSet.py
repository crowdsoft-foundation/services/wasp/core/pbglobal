from .abstract import Event

class taskReminderSet(Event):
    def __init__(self, owner=None, creator=None, correlation_id=None, create_time=None, task_id=None, reminder_id=None, reminder_type=None, seconds_in_advance=None, recipients=None, skip_validation=False):
        super().__init__()
        self.owner = owner
        self.creator = creator
        self.correlation_id = correlation_id
        self.create_time = create_time

        self.task_id = task_id
        self.reminder_id = reminder_id
        self.reminder_type = reminder_type
        self.seconds_in_advance = seconds_in_advance
        self.recipients = recipients

        if not skip_validation:
            self.validate()


    def validate(self):
        assert self.reminder_id is not None, "reminder_id may not be empty"
        assert self.task_id is not None, "event_id may not be empty"
        assert self.reminder_type is not None, "reminder_type may not be empty"
        assert self.seconds_in_advance is not None, "state may not be empty"
        return True
