from .abstract import Event

class easy2scheduleReminderEmailConfigurationSet(Event):
    def __init__(self, owner=None, creator=None, data=None, correlation_id=None, skip_validation=False, amqp_client=None):
        super().__init__(amqp_client=amqp_client)

        self.owner = owner
        self.creator = creator
        self.correlation_id = correlation_id
        self.data = data

        if not skip_validation:
            self.validate()

    def validate(self):
        assert self.owner is not None, "owner may not be empty"
        assert self.creator is not None, "creator may not be empty"
        assert self.correlation_id is not None, "correlation_id may not be empty"
        assert self.data is not None, "data may not be empty"
        return True
