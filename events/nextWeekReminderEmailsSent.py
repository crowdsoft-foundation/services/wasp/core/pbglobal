from .abstract import Event

class nextWeekReminderEmailsSent(Event):
    def __init__(self, recipients = [], skip_validation=True, amqp_client=None):
        if amqp_client is not None:
            super().__init__(amqp_client=amqp_client)
        else:
            super().__init__()

        self.consumer = "system"
        self.owner = "system"
        self.creator = "system"
        self.login = "system"
        self.correlation_id = "system"
        self.recipients = recipients

        if not skip_validation:
            self.validate()

    def validate(self):
        return True
