from .abstract import Event


class calendarRessourceDeleted(Event):
    def __init__(self, ressource_id=None, skip_validation=False):
        super().__init__()
        self.ressource_id = ressource_id

        if not skip_validation:
            self.validate()

    def validate(self):
        assert self.ressource_id is not None, "ressource_id may not be empty"
        return dict(result=True)
