from .abstract import Event
import datetime
import hashlib

class queueUpdated(Event):
    def __init__(self, owner_id=None, queue_id=None, queue_data=None, data_owners=None, correlation_id=None, skip_validation=False,
                 amqp_client=None):
        if amqp_client is not None:
            super().__init__(amqp_client=amqp_client)
        else:
            super().__init__()

        self.owner = owner_id
        self.queue_data = queue_data
        self.queue_id = queue_id
        self.correlation_id = correlation_id
        self.data_owners = data_owners

        if not skip_validation:
            self.validate()

    def validate(self):
        assert self.owner is not None, "consumer may not be empty"
        assert self.creator is not None, "login may not be empty"
        assert self.correlation_id is not None, "correlation_id may not be empty"
        assert self.queue_id is not None and len(self.queue_id) > 0, "queue_id may not be empty"
        assert self.queue_data is not None, "queue_data may not be empty"
        assert self.queue_data is not dict, "queue_data must be an object"
        assert self.queue_data.get("queue_name") is not None, "self.queue_data.queue_name must be set"

        return True
