from .abstract import Event


class newTaskManagerTemplateGroupCreated(Event):
    def __init__(self, owner_id=None, scheduling_interval=None, template_group_data=None, group_id=None, data_owners=None, start_date=None, correlation_id=None, skip_validation=False, amqp_client=None):
        if amqp_client is not None:
            super().__init__(amqp_client=amqp_client)
        else:
            super().__init__()

        self.owner = owner_id
        self.scheduling_interval = scheduling_interval
        self.template_group_data = template_group_data
        self.group_id = group_id
        self.correlation_id = correlation_id
        self.data_owners = data_owners
        self.start_date = start_date

        if not skip_validation:
            self.validate()

    def validate(self):
        assert self.owner is not None, "consumer may not be empty"
        assert self.creator is not None, "creator may not be empty"
        assert self.correlation_id is not None, "correlation_id may not be empty"
        assert self.template_group_data is not None, "template_group_data may not be empty"
        assert self.template_group_data is not dict, "template_group_data must be an object"
        assert self.template_group_data.get("name") is not None, "self.template_group_data.name must be set"

        return True