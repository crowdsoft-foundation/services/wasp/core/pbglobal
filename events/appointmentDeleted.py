from .abstract import Event


class appointmentDeleted(Event):
    def __init__(self, event_id=None, owner=None, creator=None, correlation_id=None, skip_validation=False):
        super().__init__()
        self.event_id = event_id
        self.owner = owner
        self.creator = creator
        self.correlation_id = correlation_id
        if not skip_validation:
            self.validate()

    def validate(self):
        assert self.event_id is not None, "event_id may not be empty"
        assert self.owner is not None, "owner may not be empty"
        assert self.creator is not None, "password may not be empty"
        assert self.correlation_id is not None, "correlation_id may not be empty"
        return True
