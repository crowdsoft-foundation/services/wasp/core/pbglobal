from .abstract import Event
import datetime
import hashlib

class contactDeleted(Event):
    def __init__(self, consumer_id=None, contact_id=None, external_id=None, correlation_id=None, skip_validation=False,
                 amqp_client=None):
        if amqp_client is not None:
            super().__init__(amqp_client=amqp_client)
        else:
            super().__init__()

        self.consumer_id = consumer_id
        self.owner_id = consumer_id
        self.contact_id = contact_id
        self.external_id = external_id
        self.correlation_id = correlation_id

        if not skip_validation:
            self.validate()

    def validate(self):
        assert self.consumer_id is not None, "consumer_id may not be empty"
        assert self.owner_id is not None, "owner_id may not be empty"
        assert self.contact_id is not None, "contact_id may not be empty"
        assert self.correlation_id is not None, "correlation_id may not be empty"

        return True
