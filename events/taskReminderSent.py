from .abstract import Event

class taskReminderSent(Event):
    def __init__(self, owner=None, creator=None, correlation_id=None, create_time=None, reminder_id=None, skip_validation=False):
        super().__init__()
        self.owner = owner
        self.creator = creator
        self.correlation_id = correlation_id
        self.create_time = create_time
        self.reminder_id = reminder_id

        if not skip_validation:
            self.validate()


    def validate(self):
        assert self.reminder_id is not None, "reminder_id may not be empty"
        return True
