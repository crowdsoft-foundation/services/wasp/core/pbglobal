from .abstract import Event
import datetime
import hashlib

class easy2scheduleReminderDeleted(Event):
    def __init__(self, owner_id=None, reminder_id=None, queue_data=None, correlation_id=None, skip_validation=False,
                 amqp_client=None):
        if amqp_client is not None:
            super().__init__(amqp_client=amqp_client)
        else:
            super().__init__()

        self.owner = owner_id
        self.reminder_id = reminder_id
        self.correlation_id = correlation_id

        if not skip_validation:
            self.validate()

    def validate(self):
        assert self.owner is not None, "consumer may not be empty"
        assert self.creator is not None, "login may not be empty"
        assert self.correlation_id is not None, "correlation_id may not be empty"
        assert self.reminder_id is not None and len(self.reminder_id) > 0, "reminder_id may not be empty"

        return True
