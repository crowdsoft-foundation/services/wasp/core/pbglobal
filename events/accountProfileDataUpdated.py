from .abstract import Event
import datetime
import hashlib

class accountProfileDataUpdated(Event):
    def __init__(self, consumer_id=None, account_id=None, data=None, correlation_id=None, skip_validation=False, amqp_client=None):
        if amqp_client is not None:
            super().__init__(amqp_client=amqp_client)
        else:
            super().__init__()

        self.owner_id = consumer_id
        self.account_id = account_id
        self.data = data
        self.correlation_id = correlation_id

        if not skip_validation:
            self.validate()

    def validate(self):
        assert self.owner_id is not None, "owner_id may not be empty"
        assert self.account_id is not None, "account_id may not be empty"
        assert self.correlation_id is not None, "correlation_id may not be empty"
        assert self.data is not None, "data may not be empty"
        assert self.data is not dict, "data must be an object"

        return True
