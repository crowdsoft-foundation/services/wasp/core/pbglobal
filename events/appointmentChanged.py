from .abstract import Event


class appointmentChanged(Event):
    def __init__(self,
                 event_id=None,
                 resourceId=None,
                 textColor=None,
                 color=None,
                 attachedRessources=None,
                 starttime=None,
                 endtime=None,
                 title=None,
                 savekind=None,
                 extended_props=None,
                 allDay=None,
                 correlation_id=None,
                 owner=None,
                 creator=None,
                 creator_consumer_name=None,
                 skip_validation=False,
                 attachedContacts=None,
                 data_owners=None,
                 reminders=None
                 ):
        super().__init__()
        self.event_id = None
        self.resourceId = None
        self.textColor = None
        self.color = None
        self.attachedRessources = None
        self.starttime = None
        self.endtime = None
        self.title = None
        self.savekind = None
        self.extended_props = None
        self.allDay = None
        self.correlation_id = None
        self.owner = None
        self.creator = None
        self.creator_consumer_name = None
        self.correlation_id = None
        self.data_owners = []
        self.attachedContacts = []
        self.reminders = []

        if not skip_validation:
            self.validate()


    def validate(self):
        assert self.event_id is not None, "event_id may not be empty"
        assert self.resourceId is not None, "resourceId may not be empty"
        assert self.textColor is not None, "textColor may not be empty"
        assert self.color is not None, "color may not be empty"
        assert self.attachedRessources is not None, "attachedRessources may not be empty"
        assert self.starttime is not None, "starttime may not be empty"
        assert self.endtime is not None, "endtime may not be empty"
        assert self.title is not None, "title may not be empty"
        assert self.savekind is not None, "savekind may not be empty"
        assert self.extended_props is not None, "extended_props may not be empty"
        assert self.allDay is not None, "allDay may not be empty"
        assert self.owner is not None, "owner may not be empty"
        assert self.creator is not None, "creator may not be empty"
        assert self.creator_consumer_name is not None, "creator_consumer_name may not be empty"
        assert self.correlation_id is not None, "correlation_id may not be empty"
        return True
