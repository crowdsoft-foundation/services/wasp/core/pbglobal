from .abstract import Event

class rightsRoleDeleted(Event):
    def __init__(self, owner=None, creator=None, name=None, correlation_id=None, skip_validation=False, amqp_client=None):
        super().__init__(amqp_client=amqp_client)

        self.owner = owner
        self.creator = creator
        self.name = name
        self.correlation_id = correlation_id

        if not skip_validation:
            self.validate()

    def validate(self):
        assert self.owner is not None, "owner may not be empty"
        assert self.name is not None, "name may not be empty"
        assert self.correlation_id is not None, "correlation_id may not be empty"
        return True
