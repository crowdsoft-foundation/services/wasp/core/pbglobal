from .abstract import Event


class accountCreated(Event):
    def __init__(self, username=None, password=None, email=None, roles=[], correlation_id=None, skip_validation=False):
        super().__init__()
        self.username = username
        self.password = password
        self.correlation_id = correlation_id
        self.firstname = ""
        self.lastname = ""
        self.company = ""
        self.city = ""
        self.street = ""
        self.zipcode = ""
        self.phone = ""
        self.email = email
        self.roles = roles

        if not skip_validation:
            self.validate()

    def validate(self):
        assert self.username is not None, "username may not be empty"
        assert self.password is not None, "password may not be empty"
        assert self.email is not None, "email may not be empty"
        assert self.correlation_id is not None, "correlation_id may not be empty"
        return True
