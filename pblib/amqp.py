import pika
import threading
from os import environ
from time import sleep
import traceback
from .decorators import singleton
import uuid
import json
from config import Config
import sqlalchemy
import os
import signal

def getAmqpData():
    host = environ.get("MQ_HOST")
    username = environ.get("MQ_USERNAME")
    password = environ.get("MQ_PASSWORD")
    port = int(environ.get("MQ_PORT")) if environ.get("MQ_PORT") is not None else 5672
    vhost = environ.get("MQ_VHOST") if environ.get("MQ_VHOST") is not None else "/"

    if host is None or username is None or password is None:
        return {}

    result = {'host': host, 'user': username, 'password': password, 'port': port, 'vhost':vhost}
    return result

@singleton
class broadcasting_handlers():
    currentNetwork = []

    def killYourself(self, ch, msg, name, method):
        if name in json.loads(msg).get("recipients"):
            ch.basic_ack(method.delivery_tag)
            sleep(1)
            for i in range(0, 30):
                os.kill(i, signal.SIGTERM)
            return True

    def whosThere(self, ch, msg, name, method):
        self.currentNetwork = []
        payload = {"consumer_name": name}
        client().publish(toExchange="broadcasting", routingKey="consumerListening", message=json.dumps(payload))
        return True

    def consumerListening(self, ch, msg, name, method):
        self.currentNetwork.append(json.loads(msg).get("consumer_name"))
        print("These are consumers who are listening", json.dumps(self.currentNetwork))
        return True

    def truncateDatabases(self, ch, msg, name, method):
        try:
            if name in json.loads(msg).get("recipients"):
                result = Config.db_session.execute(sqlalchemy.text(
                    "SELECT TABLE_NAME FROM information_schema.tables where table_schema = :db_name"), {'db_name': Config.db_name})

                for table in result:
                    table_name, = table
                    print("Truncating database-table:", table_name)
                    result = Config.db_session.execute(sqlalchemy.text(f"TRUNCATE TABLE {table_name}"))

                payload = {"consumer_name": name}
                client().publish(toExchange="broadcasting", routingKey="databasesTruncated", message=json.dumps(payload))
                return True
            else:
                print("Skipping truncateDatabases message because I'm not in the recipients-list", msg)
                return True

        except Exception as e:
            print("Exception:", e)
            sleep(3)
            return False


@singleton
class broadcasting():
    def __init__(self):
        pass

    def consume(self, client, broadcast_for):
        try:
            connection = client.getConnection(client)
            channel = client.getChannel(client, connection)
            channel.exchange_declare(exchange='broadcasting', exchange_type='fanout')
            if len(broadcast_for) > 0 and "_broadcast_" not in broadcast_for:
                broadcast_for_unique = broadcast_for + "_broadcast_" + str(uuid.uuid4())
            result = channel.queue_declare(queue=broadcast_for_unique, exclusive=True)
            queue_name = result.method.queue
            channel.queue_bind(exchange='broadcasting', queue=queue_name)

            def callback(ch, method, properties, body):
                event=method.routing_key
                if (event is not None):
                    handler_exists = method.routing_key in dir(broadcasting_handlers())

                    if handler_exists is True:
                        if getattr(broadcasting_handlers(), method.routing_key)(ch, body, broadcast_for_unique, method) == True:
                            pass
                            #ch.basic_ack(method.delivery_tag)
                        else:
                            pass
                            #ch.basic_nack(method.delivery_tag)
                body = body.decode("utf-8")

            channel.basic_consume(queue=queue_name, on_message_callback=callback, auto_ack=True)
            channel.start_consuming()

        except Exception as e:
            print("Exception", e)
            sleep(3)
            broadcasting().consume(client, broadcast_for)


    def signUpOnBroadcasting(self, client, broadcast_for=""):
        t = threading.Thread(target=self.consume, name="broadcasting_process", args=([client, broadcast_for]))
        t.daemon = True
        t.start()


class client():
    broadcasting_connected=False
    def publish(client, toExchange, routingKey, message, expiration=30000):
        connection = client.getConnection()
        channel = client.getChannel(connection)
        if expiration is not None:
            properties = pika.BasicProperties(
                expiration=f"{expiration}",
            )
            channel.basic_publish(exchange=toExchange, routing_key=routingKey, body=str(message), properties=properties)
        else:
            channel.basic_publish(exchange=toExchange, routing_key=routingKey, body=str(message))
        #print(" [x] Sent: " + str(message))

        connection.close()

    def addConsumer(name, queue, bindings, callback, auto_connect_broadcasting=True):
            if auto_connect_broadcasting is True and client.broadcasting_connected is False:
                broadcasting().signUpOnBroadcasting(client, queue)
                client.broadcasting_connected = True
            t = threading.Thread(target=client.receive,name=name,args=([name, queue, bindings, callback]))
            t.daemon = True
            t.start()

    def receive(name, queue, bindings, callback):
        try:
            connection = client.getConnection(client)
            channel = client.getChannel(client,connection)
            channel.add_on_cancel_callback(client.onCancel)

            for fromExchange in bindings:
                client.setupChannel(client,channel, queue, fromExchange)
                for withRoutingKeys in bindings[fromExchange]:
                    client.addBinding(channel, queue, fromExchange, withRoutingKeys)

            channel.basic_consume(queue, callback, consumer_tag=name)
            print(' [*] Waiting for messages. To exit press CTRL+C')
        

            channel.start_consuming()
        except Exception as error:
            if(str(error) == 'onCancel'):
                print('Got a basic.cancel message from amqp. Trying to restart the consumer...')
            else:
                print(traceback.format_exc())
                print(error)
                print("Consumer broke unexpectedly, trying to restart the consumer...")


            print("Restarting consumer...")
            sleep(3)
            client.addConsumer(name, queue, bindings, callback)
            return False


    #default callback when message is received, prints the message and ack's it
    def printReceivedMessage(ch, method=None, properties=None, body=None):
        print(" [x] Received %r" % body)
        ch.basic_ack(method.delivery_tag)
    

    ######################################################
    # From now on, the functions are mainly internal     #
    # Make sure you know what you're doing if you change #
    # something behind this point                        #
    ######################################################

    def getCredentials(client):
        data = getAmqpData()
        credentials = pika.PlainCredentials(data['user'], data['password'])
        return credentials

    def getParameters(param):
        data = getAmqpData()
        parameters = pika.ConnectionParameters(data['host'], data['port'], data['vhost'],  client.getCredentials(param))
        return parameters

    def getConnection(param):
        parameters = client.getParameters(param)
        connection = pika.BlockingConnection(parameters)
        return connection

    def getChannel(client,connection):
        channel = connection.channel()
        return channel

    def setupChannel(client,channel, queue, fromExchange,exchange_type='topic'):
        channel.exchange_declare(exchange=fromExchange, exchange_type=exchange_type, passive=False, durable=True, auto_delete=False, internal=False)
        #channel.queue_delete(queue=queue)

        arguments = { "x-dead-letter-exchange": fromExchange + "_dead"}
        channel.queue_declare(queue=queue, arguments=arguments)

        channel.basic_qos(prefetch_count=1)

        channel.exchange_declare(exchange=fromExchange + "_dead", exchange_type=exchange_type, passive=False, durable=True,
                                 auto_delete=False, internal=False)
        channel.queue_declare(queue=queue + "_dead")
        channel.basic_qos(prefetch_count=1)

    def addBinding(channel, queue, fromExchange, withRoutingKeys):
        channel.queue_bind(queue=queue, exchange=fromExchange, routing_key=withRoutingKeys)
        channel.queue_bind(queue=queue + "_dead", exchange=fromExchange + "_dead", routing_key=withRoutingKeys)

    def onCancel(channel):
        raise Exception('onCancel')
    
        