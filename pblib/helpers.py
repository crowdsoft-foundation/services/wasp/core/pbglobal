import requests
import json
import os
import traceback
import functools
import secrets
import string

def singleton(class_):
    instances = {}
    def getinstance(*args, **kwargs):
        if class_ not in instances:
            instances[class_] = class_(*args, **kwargs)
        return instances[class_]
    return getinstance


def filter_dict(filter, dictionary):

    for key, value in filter.items():
        if isinstance(value, dict):
            # get node or create one
            node = dictionary.setdefault(key, {})
            filter_dict(value, node)
        else:
            dictionary[key] = value

    return dictionary


def list_diff(a, b):
    return [x for x in a if x not in b]

def stacktrace(func):
    INDENT = 4 * ' '
    @functools.wraps(func)
    def wrapped(*args, **kwds):
        # Get all but last line returned by traceback.format_stack()
        # which is the line below.
        callstack = '\n'.join([INDENT+line.strip() for line in traceback.format_stack()][:-1])
        print('{}() called:'.format(func.__name__))
        print(callstack)
        return func(*args, **kwds)

    return wrapped

def get_consumer_id_by_custom_id(custom_id):
    url = os.environ.get("APIGATEWAY_ADMIN_INTERNAL_BASEURL") + "/consumers?custom_id=" + str(custom_id)

    payload = {}
    headers = {
        'Content-Type': "application/json",
        'Cache-Control': "no-cache"
    }

    response = requests.request("GET", url, data=json.dumps(payload), headers=headers)
    #print("ConsumerID: ", response.json().get("data",{})[0].get("id"))
    returnvalue = custom_id
    if len(response.json().get("data",{})) > 0:
        returnvalue = response.json().get("data",{})[0].get("id")
    return returnvalue

def generate_password(length=15):
    alphabet = string.ascii_letters + string.digits + '!@#$%^&*()'
    while True:
        password = ''.join(secrets.choice(alphabet) for i in range(length))
        if (any(c.islower() for c in password)
                and any(c.isupper() for c in password)
                and sum(c.isdigit() for c in password) >= 3):
            break
    return password