import os
from config import Config
import requests
import json

class Kong():
    def __init__(self, apikey="", api_url=os.getenv("APIGATEWAY_INTERNAL_BASEURL"), api_admin_url=os.environ.get('APIGATEWAY_ADMIN_INTERNAL_BASEURL')):
        self.apikey = apikey
        self.api_url = api_url
        self.api_admin_url = api_admin_url

    def reset_password_of_login(self, consumer_id, login, password):
        url = f"{self.api_admin_url}/consumers/{consumer_id}/basic-auth/{login}"

        payload = {
            "password": password
        }
        headers = {
            'Content-Type': "application/json",
            'Cache-Control': "no-cache"
        }

        response = requests.request("PATCH", url, data=json.dumps(payload), headers=headers)
        print("Password updated for consumer: ", consumer_id, url)

        return response.json()