import os
import requests
import json

class AutorunBasic:

    def __init__(self):
        self.ENVIRONMENT = os.getenv("ENVIRONMENT")
        self.kong_api = os.getenv("APIGATEWAY_ADMIN_INTERNAL_BASEURL")
        print("Detected environment:", self.ENVIRONMENT)
        print("Using " + str(self.kong_api) + "as api-gateway")

    def post_deployment(self):
        pass

    def add_service(self, service_name, service_url):
        payload = {
            "name": service_name,
            "url": service_url
        }
        headers = {
            'Content-Type': "application/json",
            'cache-control': "no-cache",
        }
        url = self.kong_api + "/services"
        response = requests.request("GET", url, headers=headers)

        services = response.json()
        for service in services.get("data", []):
            if service_name == service.get("name"):
                print("Service already exists")
                return

        self.post(api_path="/services", payload=payload, headers=headers)

    def add_route(self, service_name, path, plugins=["key-auth"], strip_path=True, delete_before_create=False):
        payload = {
            "paths": [path],
            "strip_path": strip_path,
        }
        headers = {
            'Content-Type': "application/json",
            'cache-control': "no-cache",
        }

        url = self.kong_api + "/services/" + service_name + "/routes/"

        response = requests.request("GET", url, headers=headers)

        routes = response.json()

        route_id = None
        path_exists = False
        for route in routes.get("data", []):
            if path in route.get("paths"):
                print("Path already exists")
                route_id = route.get("id")
                if delete_before_create is True:
                    url = self.kong_api + f"/routes/{route_id}"
                    response = requests.request("DELETE", url)

                    print("DELETING ROUTE RESULT", response.status_code)
                else:
                    path_exists = True

        if path_exists is False:
            reply = self.post(api_path="/services/" + service_name + "/routes", payload=payload, headers=headers)
            route_id = reply.get("id")

        for plugin in plugins:
            if type(plugin) == str:
                url = self.kong_api + "/routes/" + route_id + "/plugins"
                payload = {"name": plugin}
                headers = {
                    'Content-Type': "application/json",
                    'cache-control': "no-cache",
                }

            if type(plugin) == dict:
                if plugin.get("type", "") == "acl":
                    url = self.kong_api + "/routes/" + str(route_id) + "/plugins"
                    payload = {
                        "name": "acl",
                        "config": {
                            "allow": plugin.get("allow")
                        }
                    }
                    print("PAYLOAD", payload)
                    headers = {
                        'Content-Type': "application/json",
                        'cache-control': "no-cache",
                    }
                    print("ADDING PLUGIN TO", url, payload)

            try:
                response = requests.request("POST", url, data=json.dumps(payload), headers=headers)

                if response.status_code == 409:
                    print("Plugin entry already exists")
                else:
                    print(response.text)

            except Exception:
                pass

        return route_id

    def post(self, api_path, payload, headers):
        if type(payload) == dict:
            payload = json.dumps(payload)

        url = self.kong_api + api_path
        response = requests.request("POST", url, data=payload, headers=headers)
        print(response.text)
        return response.json()