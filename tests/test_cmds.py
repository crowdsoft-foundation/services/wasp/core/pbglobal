import pytest
from pbglobal.commands.createNewCertificate import createNewCertificate
from pbglobal.commands.addContact import addContact

test_data = {}


class test_amqp_client():
    def publish(self, exchange, routing_key, message):
        global test_data
        test_data["test_amqp_client_publish"] = (exchange, routing_key, message)


def test_createNewCertificate_valid_data():
    global test_data
    command = createNewCertificate(amqp_client=test_amqp_client, consumer="Foobar", public="PUBLICKEY",
                                   private="PRIVATEKEY",
                                   addedtime="123456", correlation_id="999")
    result = command.publish()
    assert result is True


def test_createNewCertificate_invalid_data():
    global test_data
    with pytest.raises(AssertionError) as excinfo:
        command = createNewCertificate(amqp_client=test_amqp_client, public="PUBLICKEY", private="PRIVATEKEY",
                                       addedtime="123456", correlation_id="999")
        result = command.publish()


def test_addContact_valid_data():
    command = addContact(amqp_client=test_amqp_client, consumer="foobar", login="foobar", email="foobar@foobar.com", addedtime="12345",
                         external_id="my_external_id", correlation_id=9999, form_data="foobar")

    result = command.publish()
    assert result is True
