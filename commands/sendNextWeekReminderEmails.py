from .abstract import Command
import datetime
import hashlib
import json
from pbglobal.pblib.pbrsa import encrypt

class sendNextWeekReminderEmails(Command):
    def __init__(self, skip_validation=True, amqp_client=None):
        if amqp_client is not None:
            super().__init__(amqp_client=amqp_client)
        else:
            super().__init__()

        self.consumer = "system"
        self.owner = "system"
        self.creator = "system"
        self.login = "system"

        if not skip_validation:
            self.validate()

    def validate(self):
        return True
