from .abstract import Command
import datetime

class queueRemoveDataOwner(Command):
    def __init__(self, owner=None, creator=None, data=None, correlation_id=None, skip_validation=False, amqp_client=None):
        if amqp_client is not None:
            super().__init__(amqp_client=amqp_client)
        else:
            super().__init__()

        self.owner = owner
        self.creator = creator
        self.data = data
        self.correlation_id = correlation_id

        if not skip_validation:
            self.validate()

    def validate(self):
        assert self.owner is not None, "owner may not be empty"
        assert self.creator is not None, "creator may not be empty"
        assert self.correlation_id is not None, "correlation_id may not be empty"
        assert self.data is not None, "data may not be empty"

        return True
