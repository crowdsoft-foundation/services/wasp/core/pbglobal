from .abstract import Command
import datetime
import hashlib
import json
from pbglobal.pblib.pbrsa import encrypt
import uuid

class deleteQueue(Command):
    def __init__(self, owner_id=None, queue_id=None, correlation_id=None, skip_validation=False, amqp_client=None):
        if amqp_client is not None:
            super().__init__(amqp_client=amqp_client)
        else:
            super().__init__()

        self.owner = owner_id
        self.queue_id = queue_id
        self.correlation_id = correlation_id

        if not skip_validation:
            self.validate()

    def validate(self):
        assert self.owner is not None, "consumer may not be empty"
        assert self.creator is not None, "login may not be empty"
        assert self.correlation_id is not None, "correlation_id may not be empty"
        assert self.queue_id is not None and len(self.queue_id) > 0, "queue_id may not be empty"

        return True
