from .abstract import Command
import datetime
import hashlib
import json
from pbglobal.pblib.pbrsa import encrypt
import uuid

class createNewContact(Command):
    def __init__(self, consumer_id=None, owner_id=None, data=None, external_id=None, correlation_id=None, data_owners=[], parent_ids=[], child_ids=[], skip_validation=False, amqp_client=None):
        if amqp_client is not None:
            super().__init__(amqp_client=amqp_client)
        else:
            super().__init__()

        self.consumer_id = consumer_id
        self.owner_id = owner_id
        self.contact_id = str(uuid.uuid4())
        self.data = data
        self.external_id = external_id
        self.correlation_id = correlation_id
        self.data_owners = data_owners
        self.parent_ids = parent_ids
        self.child_ids = child_ids

        if not skip_validation:
            self.validate()

    def validate(self):
        assert self.consumer_id is not None, "consumer_id may not be empty"
        assert self.owner_id is not None, "owner_id may not be empty"
        assert self.contact_id is not None, "contact_id may not be empty"
        assert self.correlation_id is not None, "correlation_id may not be empty"
        assert self.data is not None, "data may not be empty"
        assert self.data is not dict, "data must be an object"

        return True
