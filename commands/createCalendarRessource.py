from .abstract import Command


class createCalendarRessource(Command):
    def __init__(self,
                 display_name=None,
                 type="person",
                 props=None,
                 correlation_id=None,
                 owner=None,
                 creator=None,
                 skip_validation=False
                 ):
        super().__init__()

        self.display_name = display_name
        self.type = type
        self.props = props
        self.correlation_id = correlation_id
        self.owner = owner
        self.creator = creator
        self.correlation_id = correlation_id

        if not skip_validation:
            self.validate()


    def validate(self):
        assert self.display_name is not None, "display_name may not be empty"

        return True
