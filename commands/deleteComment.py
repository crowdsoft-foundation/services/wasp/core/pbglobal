from .abstract import Command


class deleteComment(Command):
    def __init__(self, comment_id=None, skip_validation=False):
        super().__init__()
        self.comment_id = comment_id
        if not skip_validation:
            self.validate()

    def validate(self):
        assert self.comment_id is not None, "comment_id may not be empty"
        return dict(result=True)
