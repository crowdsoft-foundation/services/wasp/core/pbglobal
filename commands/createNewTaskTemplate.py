from .abstract import Command
import uuid


class createNewTaskTemplate(Command):
    def __init__(self, owner_id=None, template_data=None, data_owners=None, correlation_id=None, skip_validation=False, amqp_client=None):
        if amqp_client is not None:
            super().__init__(amqp_client=amqp_client)
        else:
            super().__init__()

        self.owner = owner_id
        self.template_data = template_data
        self.template_id = str(uuid.uuid4())
        self.correlation_id = correlation_id
        self.data_owners = data_owners

        if not skip_validation:
            self.validate()

    def validate(self):
        assert self.owner is not None, "consumer may not be empty"
        assert self.creator is not None, "creator may not be empty"
        assert self.correlation_id is not None, "correlation_id may not be empty"
        assert self.template_data is not None, "template_data may not be empty"
        assert self.template_data is not dict, "template_data must be an object"
        assert self.template_data.get("queue_id") is not None and len(self.template_data.get("queue_id")) > 0, "queue_id may not be empty"
        assert self.template_data.get("title") is not None, "self.template_data.title must be set"

        return True
