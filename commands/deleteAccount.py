from .abstract import Command


class deleteAccount(Command):
    def __init__(self, id=None, skip_validation=False):
        super().__init__()
        self.id = id

        if not skip_validation:
            self.validate()

    def validate(self):
        assert self.id is not None, "id may not be empty"
        return dict(result=True)
