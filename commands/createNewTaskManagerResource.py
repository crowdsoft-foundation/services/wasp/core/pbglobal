from .abstract import Command
import uuid


class createNewTaskManagerResource(Command):
    def __init__(self, owner_id=None, type="room", resource_data=None, data_owners=None, correlation_id=None, skip_validation=False, amqp_client=None):
        if amqp_client is not None:
            super().__init__(amqp_client=amqp_client)
        else:
            super().__init__()

        self.owner = owner_id
        self.type = type
        self.resource_data = resource_data
        self.resource_id= str(uuid.uuid4())
        self.correlation_id = correlation_id
        self.data_owners = data_owners

        if not skip_validation:
            self.validate()

    def validate(self):
        assert self.owner is not None, "consumer may not be empty"
        assert self.creator is not None, "creator may not be empty"
        assert self.correlation_id is not None, "correlation_id may not be empty"
        assert self.resource_data is not None, "resource_data may not be empty"
        assert self.resource_data is not dict, "resource_data must be an object"
        assert self.resource_data.get("name") is not None, "self.template_data.name must be set"

        return True