from .abstract import Command
import datetime
import hashlib
import json
from pbglobal.pblib.pbrsa import encrypt

class createNewCertificate(Command):
    def __init__(self, amqp_client=None, consumer=None, public=None, private=None, addedtime=None, correlation_id=None, skip_validation=False):
        if amqp_client is not None:
            super().__init__(amqp_client=amqp_client)
        else:
            super().__init__()
        self.owner = consumer
        self.creator = consumer
        self.public = public
        self.private = private
        self.addedtime = addedtime

        self.correlation_id = correlation_id
        if not skip_validation:
            self.validate()

    def validate(self):
        assert self.owner is not None, "consumer may not be empty"
        assert self.public is not None, "public may not be empty"
        assert self.private is not None, "private may not be empty"
        assert self.addedtime is not None, "addedtime may not be empty"
        assert self.correlation_id is not None, "correlation_id may not be empty"
        return True

