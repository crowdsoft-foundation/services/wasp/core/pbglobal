from .abstract import Command
import datetime
import hashlib
import json
from pbglobal.pblib.pbrsa import encrypt

class removeContact(Command):
    def __init__(self, consumer=None, login=None, email=None, deletedtime=None, external_id=None, correlation_id=None, skip_validation=False, form_data=None):
        super().__init__()
        self.consumer = consumer
        self.login = login
        self.form_data = form_data
        self.deletedtime = deletedtime
        self.external_id = external_id
        self.form_data_hash = "" if self.form_data is None else hashlib.md5(self.form_data.encode('utf-8')).hexdigest()

        self.correlation_id = correlation_id
        if not skip_validation:
            self.validate()

    def validate(self):
        assert self.consumer is not None, "consumer may not be empty"
        assert self.login is not None, "login may not be empty"
        assert self.deletedtime is not None, "datetime may not be empty"
        assert self.correlation_id is not None, "correlation_id may not be empty"
        if type(self.form_data) is dict:
            self.form_data = json.dumps(self.form_data)
            self.form_data_hash = "" if self.form_data is None else hashlib.md5(
                self.form_data.encode('utf-8')).hexdigest()
            self.form_data = encrypt(self.form_data)
        elif not len(self.form_data_hash) > 0:
            self.form_data_hash = "" if self.form_data is None else hashlib.md5(
                self.form_data.encode('utf-8')).hexdigest()
        return True
