from .abstract import Command
import datetime
import hashlib
import json
from pbglobal.pblib.pbrsa import encrypt
import uuid

class createNewComment(Command):
    def __init__(self, owner_id=None, creator=None, data=None, data_owners=None, skip_validation=False, amqp_client=None):
        if amqp_client is not None:
            super().__init__(amqp_client=amqp_client)
        else:
            super().__init__()

        self.owner = owner_id
        self.creator = creator
        self.comment_id = str(uuid.uuid4())
        self.data = data
        self.data_owners = data_owners
        
        if not skip_validation:
            self.validate()

    def validate(self):
        assert self.owner is not None, "consumer may not be empty"
        assert self.creator is not None, "login may not be empty"
        assert self.comment_id is not None, "comment_id may not be empty"
        assert self.data.get("comment") is not None, "data.comment may not be empty"

        return True
